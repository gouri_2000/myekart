// import React from 'react';
// import axios from 'axios';
// import {useQuery, useQueryClient, useMutation, QueryClient, QueryClientProvider,} from 'react-query';
// import { ReactQueryDevtools } from 'react-query/devtools'

// const queryClient = new QueryClient()

// export default function Weather() {
//   return (
//     <QueryClientProvider client={queryClient}>
//       <Example />
//     </QueryClientProvider>
//   )
// }

// function Example() {
//   const queryClient = useQueryClient()
//   const [intervalMs, setIntervalMs] = React.useState(1000)
//   const [value, setValue] = React.useState('kolkata')
//   const [count, setCount] = React.useState(0)

//   const { status, data, error, isFetching } = useQuery(
//     'todos',
//     async () => {

//       const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${value}&appid=df4ae1cc80b9211e2a2405b66e606c28`)
//       console.log(res);
//       {setCount(count+1)}
//       return res.data
//     },
//     {
//       // Refetch the data every second
//       refetchInterval: intervalMs,
//     }
//   )

//   const addMutation = useMutation(value => fetch(`/api/data?add=${value}`), {
//     onSuccess: () => queryClient.invalidateQueries('todos'),
//   })

//   const clearMutation = useMutation(() => fetch(`/api/data?clear=1`), {
//     onSuccess: () => queryClient.invalidateQueries('todos'),
//   })

//   if (status === 'loading') return <h1>Loading...</h1>
//   if (status === 'error') return <span>Error: {error.message}</span>

//   return (
//     <div>
//       <h1>Auto Refetch with stale-time set to 1s)</h1>
//       <p>
//         This example is best experienced on your own machine, where you can open
//         multiple tabs to the same localhost server and see your changes
//         propagate between the two.
//       </p>
//       <label>
//         Query Interval speed (ms):{' '}
//         <input
//           value={intervalMs}
//           onChange={ev => setIntervalMs(Number(ev.target.value))}
//           type="number"
//           step="100"
//         />{' '}
//         <span
//           style={{
//             display: 'inline-block',
//             marginLeft: '.5rem',
//             width: 10,
//             height: 10,
//             background: isFetching ? 'green' : 'transparent',
//             transition: !isFetching ? 'all .3s ease' : 'none',
//             borderRadius: '100%',
//             transform: 'scale(2)',
//           }}
//         />
//       </label>
//       <h2>Todo List</h2>
//       <h3>Api Hit {count} Times</h3>
//       <form
//         onSubmit={event => {
//           event.preventDefault()
//           addMutation.mutate(value, {
//             onSuccess: (event) => {
//               setValue(event.target.value)
//             },
//           })
//         }}
//       >
//         <input
//           placeholder="enter something"
//           value={value}
//           onChange={(e) => setValue(e.target.value)}
//         />
//       </form>
//       <ul>
//         {data?.weather?.map(item => (
//           <li key={item.id}>{item.description}</li>
//           //console.log(item)
//         ))}
//       </ul>
//       <div>
//         <button
//           onClick={() => {
//             clearMutation.mutate()
//           }}
//         >
//           Clear All
//         </button>
//       </div>
//       <ReactQueryDevtools initialIsOpen />
//     </div>
//   )
// }





import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import "../components/Weather.css";
import axios from "axios";
import moment from "moment";
import useQuery from 'react-query';



const Weather = () => {

    const [city, setCity] = useState(null);
    const [weather, setWeather] = useState("");
    const [search, setSearch] = useState("");
    const [intervalMs, setIntervalMs] = useState();
    var myData;
    var weatherData;

    
    const [time, setTime] = useState(Date.now());

    useEffect(() => {
        const interval = setInterval(() => setTime(moment().format("DD-MM-YYYY hh:mm:ss")));
        return () => {
            clearInterval(interval);
        };
    }, [intervalMs]);

    const getResponse = async () => {
        try {
            const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${search}&units=metric&appid=df4ae1cc80b9211e2a2405b66e606c28`);
            const resJson = await (res?.data);
            myData = await (res?.data);
            weatherData = (myData['weather'][0]['main']);
            setCity(resJson.main);
            setWeather(weatherData);
            setIntervalMs(1000);
            // console.log(setWeather);
            // console.log(myData);

            console.log(weatherData);

        } catch (error) {
            console.log(error)
        }

    }

    useEffect(() => {
        getResponse();
    }, [search])



    return (
        <div className="weatherBody">
            <div className="box">
                <div className="inputData">
                    <input type="search" className="inputField mt-4" onChange={(event) => { setSearch(event.target.value) }} />
                </div>
                {!city ? (
                    <p className="errorMsg">No Data Found</p>
                ) : (
                    <div>
                        <div className="info">
                            <h2 className="location"><i className="fas fa-street-view"></i>{search}</h2>
                            <h1 className="temp">{city.temp}°Cel</h1>
                            <h3 className="tempmin_max">Min : {city.temp_min}°Cel |  |  Max : {city.temp_max}°Cel</h3>
                            <h5 className="temp_weather">Weather: {weather}</h5>
                            <h2><p>{time}</p></h2>
                        </div>
                        <div className="wave -one"></div>
                        <div className="wave -two"></div>
                        <div className="wave -three"></div>
                    </div>

                )
                }
            </div>
        </div>
    );
}

export default Weather;