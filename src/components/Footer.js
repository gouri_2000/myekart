import React from "react";

const Footer = () => {
    return (
        <div className="bd-footer py-10 mt-5 bg-light">
            <div className="container me-3">
                <div className="row">
                    <div className="col-md-3 pt-3 col-sm-6">
                        <h4>ABOUT</h4><br/>
                        <ul className="list-unstyled">
                            <li>Contact Us</li>
                            <li>About Us</li>
                            <li>Carrers</li>
                        </ul>
                    </div>
                    <div className="col-md-3 pt-3 col-sm-6">
                        <h4>Social</h4><br/>
                        <ul className="list-unstyled">
                            <li>Whatsapp</li>
                            <li>Facebook</li>
                            <li>Twitter</li>
                        </ul>
                    </div>
                    <div className="col-md-3 pt-3 col-sm-6">
                        <h4>HELP</h4><br/>
                        <ul className="list-unstyled">
                            <li>Payments</li>
                            <li>Shipping</li>
                            <li>Cancelation & Return</li>
                            <li>Report</li>
                        </ul>
                    </div>
                </div><br/>
                <div className="footer-bottom pb-3">
                    <p className="text-xs-center">
                    &copy;{new Date().getFullYear()} MY EKART - All Rights Reserved
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Footer;