import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { addCart, delCart } from "./redux/action";



const Cart = () => {
    const state = useSelector((state) => state.handelCart)
    const dispatch = useDispatch();

    const handelClose = (item) => {
        dispatch(delCart(item))
    }
    const handelAdd = (item) => {
        dispatch(addCart(item))
    }

    const cartItem = (cartItem) => {
        return (
            <div className="px-4 my-3 bg-light rounded-3">
                <div className="container py-4">
                    <div className="row">
                        <div className="col-md-5">
                            <img src={cartItem.image} alt={cartItem.title} height="100%" width="35%" />
                        </div>
                        <div className="col-md-4">
                            <h3>{cartItem.title}</h3>
                            <p className="lead fw-bold">Rs.{cartItem.price}</p>
                        </div>

                        <div className="col-md-3">
                            <div className="row">
                                <div className="col">
                                    <button onClick={() => handelAdd(cartItem)} className="btn btn-outline-secondary ms-2" aria-label="Close">Add</button>
                                </div>
                                <div className="col">
                                    <p className="fw-bold mt-2 mx-1 ms-4">{cartItem.qty}</p>
                                </div>
                                <div className="col">
                                    <button onClick={() => handelClose(cartItem)} className="btn btn-outline-danger ms-2" aria-label="Close">Delete</button>
                                </div>
                            </div>
                            <div className="row">
                                <p className="lead fw-bold mt-5 ms-2">
                                    Total Amount: {cartItem.qty * cartItem.price}
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

    const emptyCart = () => {
        return (
            <div className="px-3 my-4 bg-light rounded-3 py-5">
                <div className="container py-4">
                    <div className="row">
                        <h3 className="fw-bold text-danger text-center"> Opps!!! Your Cart is Empty</h3>
                    </div>
                </div>
            </div>
        )
    }


    const button = () => {
        return (
            <div className="container">
                <div className="row-right mb-3">
                    <NavLink to="/checkout" className="btn btn-outline-dark mb-55 w-25 ">Proceed to checkout</NavLink>
                    <NavLink to="/products" className="btn btn-outline-dark float-end mb-55 w-25 ">Add More Items</NavLink> 
                </div>
                </div>
        );
    }

    return (
        <>
            {state && state.length === 0 && emptyCart()}
            {state && state.length !== 0 && state.map(cartItem)}
            {state && state.length !== 0 && button()}
        </>
    );
}

export default Cart;