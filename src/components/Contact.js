import React from "react";


const Contact = () => {
    return(
        <div className="container" >
            <h1 className="link-danger mt-4 text-center">
                This is a Contact Page.<br/><br/>
                Having issues. <a href="mailto:support@myekart.com" target="_blank">Mail Us</a>

            </h1>
        </div>
        );
    }
export default Contact;