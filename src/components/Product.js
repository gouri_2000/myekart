import React from "react";
import { useDispatch } from "react-redux";
import { addCart, delCart } from "./redux/action";
import { NavLink, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";



const Product = () => {
    const { id } = useParams();
    const [product, setProduct] = useState([]);
    const [loading, setLoading] = useState(false);
    const [cartBtn, setCartBtn] = useState("Add to Cart")

    const dispatch = useDispatch();
    // const addProduct = (product) => {
    //     dispatch(addCart(product));
    // }
    const handelCart = (product) =>{
        if (cartBtn == "Add to Cart"){
            dispatch(addCart(product))
            setCartBtn("Remove From Cart")
        }
        else{
            dispatch(delCart(product))
            setCartBtn("Add to Cart")
        }

    }

    const getProduct = async () => {
        try {
            setLoading(true);
            const response = await axios.get(`https://fakestoreapi.com/products/${id}`);
            setProduct(response?.data);
            setLoading(false);
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getProduct();
    }, []);

    // const Loading = () =>{
    //     return<>Loading...</>;
    // };

    const ShowProducts = () => {
        return (
            <>
                <div className="col-md-6">
                    <img src={product.image} alt={product.title} height="440px" width="440px" />
                </div>
                <div className="col md-6">
                    <h4 className="text-uppercase text-black-50">
                        {product.category}
                    </h4>
                    <h1 className="display-5">{product.title}</h1>
                    <p className="lead-fw-bolder">
                        Rating {product.rating && product.rating.rate}
                        <i className="fa fa-star"/>
                    </p>
                    <h3 className="display-6 fw-bold my-4">
                        Rs.{product.price}
                    </h3>
                    <p className="lead">{product.description}</p>
                    <button className="btn btn-outline-dark" 
                    // onClick={()=>addProduct(product)}
                    onClick={()=>handelCart(product)}
                    >{cartBtn}</button>
                    <NavLink to="/cart" className="btn btn-dark ms-2 py-2">Go To Cart
                    </NavLink>
                </div>
            </>
        );
    }


    return (
        <div className="container py-5">
            <div className="row py-5">
                {/* {loading ? <Loading/> : */}
                <ShowProducts />
            </div>
        </div>
    );
}

export default Product;