import firebase from "firebase/app";
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";

  const firebaseConfig = {
    apiKey: "AIzaSyDAt8jc6oDm7EptIca89VDd0WmvSqM_fOU",
    authDomain: "eshop-2d6f0.firebaseapp.com",
    projectId: "eshop-2d6f0",
    storageBucket: "eshop-2d6f0.appspot.com",
    messagingSenderId: "184870166909",
    appId: "1:184870166909:web:1cad95892f14f055e9ac14",
    measurementId: "G-SHVSJ94GTK"
  };
  
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  // const analytics = getAnalytics(app);

  export const authentication = getAuth(app);