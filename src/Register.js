import React from "react";


const Register = () => {
    return (
        <div className="container text-center">
            <div className="row">
                <div className="mt-5">
                    <h2 className="link-success">New User Registration </h2><br/>
                    <label for="name">Full Name :</label><br />
                    <input id="name" type="text" maxLength="30" required /><br/>
                    <label for="phone" className="mt-3">Phone Number :</label><br />
                    <input id="phone" type="tel" maxLength="10" required /><br/>
                
                
                    <button className="btn btn-info ms-2 mt-3" type="submit">Register</button>
                </div>
            </div>

        </div>

    );
}
export default Register;