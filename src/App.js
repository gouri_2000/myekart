import React from 'react';
import Home from './Home';
import Login from './Login';
import Register from './Register';
import './App.css';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import About from './components/About';
import { Route, Routes } from 'react-router-dom';
import Product from './components/Product';
import Products from './components/Products';
import Contact from './components/Contact';
import Cart from './components/Cart';
import Maps from './components/Maps';
import Weather from './components/Weather';

const App = () => {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />

        <Route path="/products" element={<Products />} />
        <Route path="/product/:id" element={<Product />} />
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path='/about' element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/product" element={<Products />} />
        <Route path='/cart' element={<Cart />}/>
        <Route path='/maps' element={<Maps />}/>
        <Route path="/weather" element={<Weather />} />

      </Routes>
      <Footer />
    </div>
  );
};

export default App;
