import React, { useState } from "react";
import "./Login.css"
import { authentication } from "./firebaseConfig";
import { RecaptchaVerifier, signInWithPhoneNumber } from "firebase/auth";


const Login = () => {

    const countryCode = "+91";
    const [phoneNumber, setPhoneNumber] = useState(countryCode);
    const [expandForm, setExpandForm] = useState(false);
    const [OTP, setOTP] = useState("");

    const generateRecaptcha = () => {
        try {
            
        
        window.recaptchaVerifier = new RecaptchaVerifier('recaptcha-container', {
            'size': 'invisible',
            'callback': (response) => {
                alert("recaptchaVerifier");
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                
            }
        }, authentication);
    }catch (error) {
        console.log(error);
            
    }}

    const requestOTP = async (e) => {
        try {
            e.preventDefault();
            if (phoneNumber.length >= 12) {
                setExpandForm(true);
                generateRecaptcha();
                let appVerifier = window.recaptchaVerifier;
                signInWithPhoneNumber(authentication, phoneNumber, appVerifier).then(confirmationResult => {
                        
                        window.confirmationResult = confirmationResult;
                        console.log(appVerifier);
                      
                }).catch((error) => {
                        // Error; SMS not sent
                        
                        console.log(error);
               

                });
                
                
    
            }
        } catch (error) {
           console.log(error) 
        }
    }

    const verifyOTP = (e) => {
        // let otp = e.target.value;
        // setOTP(otp);

        if (OTP.length === 6) {
            console.log(OTP);
            let confirmationResult = window.confirmationResult;
            confirmationResult.confirm(OTP).then((result) => {
                // User signed in successfully.
                const user = result.user;
                console.log("Succesfully Verified")
                // ...
            }).catch((error) => {
                // User couldn't sign in (bad verification code?)
                // ...
                console.log(error)
            });
        }
    }

    return (
        <div className="px-3">
            <form className="py-5" onSubmit={requestOTP} >

                <div id="container"></div>
                <h2>Login With Phone Number</h2>
                <div className="mb-3">
                    <label htmlFor="phoneNumberInput" className="form-label">Phone Number :</label><br />
                    <input type="tel" id="phoneNumberInput" aria-describedby="emailHelp" value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} className="form-control" required />
                </div>
                {
                    expandForm === true?
                        <>
                            <div className="mb-3">
                                <label htmlFor="otpInput" className="form-lable">OTP</label>
                                <input type="number" className="form-control" id="otpInput" value={OTP} onChange={(e) => setOTP(e.target.value)} />

                            </div>
                        </>
                        : null
                }
                {
                    expandForm === false?
                        <button type="submit" value="Submit" className="btn btn-info ms-2" >Send OTP</button>
                        : <button type="button" className="btn btn-info ms-2" onClick={verifyOTP}>Verify OTP</button>
                }
                <div id="recaptcha-container"></div>
            </form>
        </div>
    );
}

export default Login;