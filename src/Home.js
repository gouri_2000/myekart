import React from "react";
import { NavLink } from "react-router-dom";
import Products from "./components/Products";
const Home = () => {
  return (
    <div className="container">
      <h1 className="text-lg-center mt-5 link-danger">
        This is My Ekart.<br /><br/>
        Hope You Enjoy Shopping With Us.<br/><br/>
        <NavLink to="/maps" className="btn btn-info" >See Maps</NavLink>
        <NavLink to="/weather" className="btn btn-info mx-5" >See Weather</NavLink>
      </h1>
      
      
    </div>
  );
};

export default Home;